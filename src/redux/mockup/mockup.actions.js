import MockupTypes from './mockup.types'

const mockup = (mockupPayload) => {
  return {
    type: MockupTypes.MOCK_UP,
    payload: { mockupPayload },
  }
}

const MockupActions = {
  mockup,
}

export default MockupActions

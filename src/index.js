/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react'
import {
  StyleSheet, View,
} from 'react-native'
import { AppNavigation, RegisterScreen } from './navigation'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
})

type Props = {}

RegisterScreen()

export default class App extends Component<Props> {
  constructor(props: Props) {
    super(props)
    AppNavigation.navToRoot()
  }

  render() {
    return (
      <View style={styles.container}>
      </View>
    )
  }
}

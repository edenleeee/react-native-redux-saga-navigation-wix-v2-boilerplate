import { takeLatest, call, put } from 'redux-saga/effects'
import MockupTypes from '../../redux/mockup/mockup.types';

function* doAction(action) {
  const { mockupPayload } = action.payload
  yield put({ type: MockupTypes.MOCK_UP_FAIL, payload: mockupPayload })
}

export default function* watchAction() {
  yield takeLatest(MockupTypes.MOCK_UP, doAction)
}
